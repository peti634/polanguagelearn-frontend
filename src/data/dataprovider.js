class SendBackend{

    constructor(parent){
        this.parent = parent;
    }

    

    NewDir(name){
        this.parent.cacheData.unshift({
            id: 0,
            name: name
        });
        this.parent.events["test"]("test", this.parent.getExampleData());
    }

};


class DataProvider {

    static events = [];


    static cacheData = [
        {
            id:10,
            name: "class",
            date: "asdf"
        }
    ];

    static SendBackend = new SendBackend(this);

    static getExampleData = () => {
        return this.cacheData;
    }

    static RegisterEvent(eventName, callback){
        this.events[eventName] = callback;
    }
    static UnregisterEvent(eventName){
        delete this.events[eventName];
    }
    static RequestData(size){

        const ename = "test";
        const cb = this.events[ename];
        setTimeout(() =>{cb(ename, this.getExampleData() )}, 5000);
    }

}


export default DataProvider;