import React, { Component } from 'react';

import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Button} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

import { Container, Row, Col, Card } from 'react-bootstrap';

let ws;

class websocket{

    connect(){
        const userid = localStorage.getItem("userid");
        const swURL = 'ws://user:pass@localhost:8080/echo?' + "userid=" + userid;
        ws = new WebSocket(swURL);
        ws.onopen = this.onConnect;
        ws.onmessage = this.onMessage;
        ws.onerror = this.OnError;
    }
    onError(e){
      console.info(e);
    }
  
    onConnect (){
      console.log("on connect");
    }
  
    onMessage(e){
      console.log(e.data);
    }

}