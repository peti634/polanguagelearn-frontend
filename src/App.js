

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNav from './AppNav';
import DataProvider from './data/dataprovider';
import DirList from './directories/dirlist';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import DirListItem from "./directories/diritem/diritem"

function App() {

  console.info(DataProvider.getExampleData());
  return (
    <Router>
      <div className="App">
        <AppNav />
        <Switch>
          <Route path="/dir" exact component={DirList} />
          <Route path="/dir/:id" component={DirListItem} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
