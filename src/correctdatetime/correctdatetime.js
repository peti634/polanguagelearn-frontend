import React from "react";

class CorrectDateTime extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            currDate: this.getCorrectDateTimeWithout()
        }
        this.timerID = undefined;
    }

    getCorrectDateTimeWithout(){
        const currd = new Date();
        const currds = currd.toLocaleString(navigator.language);
        const index = currds.lastIndexOf(":");
        const currdswithoutsec = currds.slice(0, index) + currds.slice(index + 3);
        return currdswithoutsec;
    }

    minUpdateCallback(){
        this.setState({ currDate:  this.getCorrectDateTimeWithout()});
    }

    componentDidMount(){
        if (this.props.minupdate === true){
            this.timerID = setInterval(this.minUpdateCallback.bind(this), 60 * 1000);
        }
    }

    componentWillUnmount() {
        if (this.timerID !== undefined){
            clearInterval(this.timerID);
        }        
    }

    render() {
        return (
            <p>{this.state.currDate}</p>
        )
    }
}

export default CorrectDateTime;