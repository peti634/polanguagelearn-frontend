import React from "react";
import { Button, Modal, Table } from "react-bootstrap";
import CorrectDateTime from "./../correctdatetime/correctdatetime"

class DirNewModal extends React.Component {
    state = {
        show: false
    };
    nameValue = "";

    handleClose = () => this.setState({ show: false });
    handleAdd = () => {
        this.setState({ show: false });
        this.props.newDirItemCallback(this.nameValue);
    }
    handleShow = () => {
        this.setState({ show: true });
    }

    render() {
        return (
            <>
                <Button variant="primary" onClick={this.handleShow}>Create new directory</Button>
                <Modal show={this.state.show} onHide={this.handleClose} centered>
                    <Modal.Header closeButton>
                        <Modal.Title>Create new directory</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Table borderless>
                            <tbody>
                                <tr>
                                    <td><label>Name</label></td>
                                    <td>
                                        <input type="text" onChange={(e) => {this.nameValue = e.target.value}} />
                                    </td>
                                </tr>
                                <tr>
                                    <td><label>Date</label></td>
                                    <td><CorrectDateTime without={true} minupdate={true} /></td>
                                </tr>
                            </tbody>
                        </Table>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.handleClose}>Cancel</Button>
                        <Button variant="primary" onClick={this.handleAdd}>Add</Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }

}

export default DirNewModal;