import { Container, Navbar, Nav, Form, FormControl, Button } from 'react-bootstrap'

export const ExampleUserInfo = ({ title, children }) => {
    return (
        <div style={{ marginLeft: '1rem', color: 'black' }}>
            <h4 style={{ marginTop: 0, marginBottom: '10px' }}>{title}</h4>
            <p style={{ marginBottom: 0 }}>{children}</p>
        </div>
    )
}

export const ExampleListItem = ({ even, children }) => {
    const cname = even ? 'DirListItemOdd' : 'DirListItemEven';
    return (
        <div className={cname}>
            {children}
        </div>
    )
}

const DirListItem = props => {

    return <ExampleListItem even={props.index % 2 === 0}>
        <Nav.Link href={"/dir/" + props.item.id}>
            <ExampleUserInfo title={props.item.name} >
                {console.info(props.item)}
                {"ez is valami"}
            </ExampleUserInfo>
        </Nav.Link>
    </ExampleListItem>
}

export default DirListItem;