import React from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { Button, Modal, Table, Image, Row, DropdownButton, Col, Container, Dropdown, Form } from "react-bootstrap";
import { Trash, PencilSquare } from 'react-bootstrap-icons';


import DirItemCUWord from "./diritemCUword"

class DirItem extends React.Component {
  state = {
    items: [{
      id: 10,
      class: "főnév",
      engWord: "cat",
      hunWord: "macska",
      example: "The cat is white",
      imgURL: "https://images.theconversation.com/files/336248/original/file-20200520-152302-97x8pw.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=1200&h=675.0&fit=crop"
    }]
  };

  incid = 11;

  fetchMoreData = () => {

  };

  onUpdate = () => {

    this.DirItemCUWord.method();
  }
  onAdd = (engWordInput, hunWordInput, wordClassInput, imgUrlInput, exampleTextInput) => {
    console.info(engWordInput, hunWordInput, wordClassInput, imgUrlInput, exampleTextInput);
    this.state.items.unshift({
      id: this.incid,
      class: wordClassInput,
      engWord: engWordInput,
      hunWord: hunWordInput,
      example: exampleTextInput,
      imgURL: imgUrlInput
    });
    this.incid++;
    this.setState({
      items: this.state.items
    });
  }


  render() {

    return (
      <div>
        <h1>Directory list:</h1>
        { console.info(this.props.match.params.id)}
        <hr className={"m-1"} />
        <DirItemCUWord 
          onRef={ref => (this.DirItemCUWord = ref)}
          onAdd={this.onAdd}
          
        />

        <hr />

        <Table striped bordered hover>
          <tbody>


          {this.state.items.map((i, index) => (
            <tr key={i.id}>
            <td><label>{index + 1}</label></td>
            <td style={{ maxWidth: "40px" }}><label>{i.class}</label></td>
            <td><label>{i.engWord}</label></td>
            <td><label>{i.hunWord}</label></td>
            <td><label>{i.example}</label></td>

            <td style={{ padding: 0 }}>
          {(i.imgURL !== undefined && i.imgURL !== "") &&  
            <Image width={"auto"} height={"150px"} src={i.imgURL} rounded />}
          </td>

            <td style={{ padding: 0 }}>
              <PencilSquare size={30} onClick={this.onUpdate} />
              <Trash size={30} />
            </td>
          </tr>
            ))}

          </tbody>
        </Table>

        <InfiniteScroll style={{ justifyContent: "center" }}
          dataLength={10}
          next={this.fetchMoreData}
          hasMore={true}
          loader={<h4>Loading...</h4>}
        >

        </InfiniteScroll>
      </div>
    );
  }
}


export default DirItem;