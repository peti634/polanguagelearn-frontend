import React from "react";
import { Button, Modal, Table, Image, Row, DropdownButton, Col, Container, Dropdown, Form } from "react-bootstrap";
import { Typeahead } from 'react-bootstrap-typeahead';
class DirItemCUWord extends React.Component {


    engWordInput = "";
    hunWordInput = "";
    wordClassInput = "";
    imgUrlInput = "";
    exampleTextInput = "";

    optionsEng = ["dog", "water", "message"];
    optionsHun = ["kutya", "víz", "üzenet"];

    componentDidMount() {
        this.props.onRef(this)
    }
    componentWillUnmount() {
        this.props.onRef(undefined)
    }

    onClickAdd = () => {
        
        this.props.onAdd(
            this.engWordInput,
            this.hunWordInput,
            this.wordClassInput,
            this.imgUrlInput,
            this.exampleTextInput
        );
        this.refTypeaheadEng.clear();
        this.refTypeaheadHun.clear();
        this.refSelectClass.selectedIndex = -1;
        this.refImageLink.value = "";
        this.refExampleText.value = "";

        this.engWordInput = "";
        this.hunWordInput = "";
        this.wordClassInput = "";
        this.imgUrlInput = "";
        this.exampleTextInput = "";

        //this.refTypeaheadEng.setState({ text: "asdf" })
    }

    render() {
        return (
            <Container style={{ width: "600px", marginLeft: "auto", marginRight: "auto" }}>
                <Row>
                    <Col><label>Create new word</label></Col>
                </Row>
                <Row>
                    <Col>
                        <Row><Col className={"m-1"}>
                            <Typeahead
                                id="idrefTypeaheadEng"
                                labelKey="engWord"
                                options={this.optionsEng}
                                placeholder="*English word..."
                                size="small"
                                ref={(ref) => this.refTypeaheadEng = ref}
                                onInputChange={(text) => { this.engWordInput = text }}
                                onChange={(textarr) => {
                                    this.engWordInput = (textarr.length > 0) ? textarr[0] : "";
                                }}
                            />
                        </Col></Row>
                        <Row><Col className={"m-1"}>
                            <Typeahead
                                id="idrefTypeaheadHun"
                                labelKey="engWord"
                                options={this.optionsHun}
                                placeholder="*Hungarian word..."
                                size="small"
                                ref={(ref) => this.refTypeaheadHun = ref}
                                onInputChange={(text) => { this.hunWordInput = text }}
                                onChange={(textarr) => {
                                    this.hunWordInput = (textarr.length > 0) ? textarr[0] : "";
                                }}
                            />
                        </Col></Row>
                        <Row><Col className={"m-1"} >
                            <Form inline>


                                <Form.Label style={{ marginRight: "auto" }}>Word class</Form.Label>
                                <Form.Control as="select" size="sm"
                                    ref={(ref) => this.refSelectClass = ref}
                                    onChange={(event) => { this.wordClassInput = event.target.value }}>
                                    <option></option>
                                    <option>Kifejezés, mondat</option>
                                    <option>Főnév</option>
                                    <option>Ige</option>
                                    <option>Melléknév</option>
                                    <option>Jelző</option>
                                </Form.Control>

                            </Form>
                        </Col></Row>
                    </Col>
                    <Col>
                        <Row>
                            <Col className={"m-1"} >
                                <Form.Control size="sm" type="email" placeholder="Image link" 
                                    ref={(ref) => this.refImageLink = ref}
                                    onChange={(event) => { this.imgUrlInput = event.target.value }}/>
                            </Col>
                        </Row>
                        <Row>
                            <Col className={"m-1"}>
                                <Form.Control size="sm" as="textarea" placeholder="Example sentence" 
                                    ref={(ref) => this.refExampleText = ref}
                                    onChange={(event) => { this.exampleTextInput = event.target.value }}/>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Button variant="secondary">Cancel</Button>
                    </Col>
                    <Col></Col>
                    <Col>
                        <Button variant="primary" onClick={this.onClickAdd}>Add</Button>
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default DirItemCUWord;