import React, { useState } from "react";
import ReactDOM from 'react-dom';
import { render } from "react-dom";
import InfiniteScroll from "react-infinite-scroll-component";
import DataProvider from './../data/dataprovider';
import { Button,Modal,Table } from "react-bootstrap";
import DirNewModal from "./dirnewmodal"
import "./dirlist.css";
import DirListItem from "./dirlistitem"





class DirList extends React.Component {
  state = {
    items: []
  };
  ScrollRef = {};
  InitLoad = true;
  RequestDirSize = 0;


  constructor() {
    super();
    
  }

  fetchMoreData = () => {
    this.RequestDirSize += 20;
    DataProvider.RequestData(this.RequestDirSize);
  };

  dataEventCallback(event, data) {
    this.setState({
      items: data
    });
  }

  componentDidMount(){
    DataProvider.RegisterEvent("test", this.dataEventCallback.bind(this));

    this.fetchMoreData();
  }

  componentWillUnmount() {
    DataProvider.UnregisterEvent("test");
  }

  newDirItemCallback(newItemName){
    DataProvider.SendBackend.NewDir(newItemName);
  }

  render() {
    return (
      <div>
        <h1>Directory list:</h1>
        <DirNewModal newDirItemCallback={this.newDirItemCallback}/>
        <hr />

        
        
        <InfiniteScroll style={{justifyContent: "center"}}
          dataLength={(this.InitLoad) ? (-1) : this.state.items.length}
          next={this.fetchMoreData}
          hasMore={true}
          loader={<h4>Loading...</h4>}
          ref={r => {this.ScrollRef = r;}}
        >
          {this.state.items.map((i, index) => (
            <div key={index}>
                <DirListItem index={index} item={i}/>
            </div>
          ))}
        </InfiniteScroll>
        
      </div>
    );
  }
}


export default DirList;