import { Link } from 'react-router-dom';
import React, { Component } from "react";
import { Container, Navbar, Nav, Form, FormControl, Button } from 'react-bootstrap'
import { GearFill } from 'react-bootstrap-icons';

class AppNav extends Component  {

  render() {
    return (
      <Navbar bg="dark" variant="dark" style={{   position: "sticky",top: 0, minWidth: 200 , minHeight: 10}}>
        <Navbar.Brand href="/">LOGO</Navbar.Brand>
        <Nav className="mr-auto">
          <Nav.Link href="/dir">Directories</Nav.Link>
          <Nav.Link href="#features">Learn</Nav.Link>
          <Nav.Link href="#pricing">Games</Nav.Link>
          
        </Nav>
        <Nav className="mr-auto">
          <Nav.Item>
            <h2>
              Hi, Iván Péter
            </h2>
          

          </Nav.Item>
          <Nav.Link href="/user/123456">
            <GearFill size={30}/>
          </Nav.Link>
        </Nav>

        <Nav className="justify-content-end">
          <Nav.Link>Logout</Nav.Link>
          
        </Nav>
    </Navbar>
    );
  }
}
  
  export default AppNav;